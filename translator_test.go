package errors_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/errors"
)

type DummyError struct{}

func (e DummyError) GetStatusCode() int       { return 400 }
func (e DummyError) GetResponse() interface{} { return e }
func (e DummyError) Error() string            { return "hello" }

func TestDefaultTranslator(t *testing.T) {
	err := DummyError{}

	translated := errors.DefaultTranslator(err)

	assert.Equal(t, err, translated)
}
