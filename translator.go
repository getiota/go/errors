package errors

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

// Translator translates a function into an HTTPError or nil
type Translator func(error) HTTPError

// DefaultTranslator handles types from echo and validator
func DefaultTranslator(err error) HTTPError {
	if e, ok := err.(HTTPError); ok {
		return e
	}

	switch t := err.(type) {
	case *echo.HTTPError:
		return NoResponse(t.Code)
	case validator.ValidationErrors:
		return ValidationErrors(t)
	default:
		return nil
	}
}
