package errors

import (
	"net/http"

	"github.com/go-playground/validator/v10"
)

// ValidationErrors is a wrapper around validator.ValidationErrors
type ValidationErrors validator.ValidationErrors

// GetStatusCode returns a bad request
func (ve ValidationErrors) GetStatusCode() int {
	return http.StatusBadRequest
}

// GetResponse returns the error detail
func (ve ValidationErrors) GetResponse() interface{} {
	return Response{
		Detail: validator.ValidationErrors(ve).Error(),
	}
}

// Error returns the validator.ValidationErrors's error
func (ve ValidationErrors) Error() string {
	return validator.ValidationErrors(ve).Error()
}
