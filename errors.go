// Package errors centralizes http error creation
package errors

import (
	"errors"
)

// HTTPError is an interface for structured errors
type HTTPError interface {
	GetStatusCode() int
	GetResponse() interface{}
}

// New is a drop in replacement for the standard errors package
var New = errors.New

// Is is a drop in replacement for the standard errors package
var Is = errors.Is
