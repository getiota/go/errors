# Errors

A simple utility for error handling in go/echo microservices

## Usage

### Echo

```go

import (
    "github.com/labstack/echo/v4"
    errorsecho "gitlab.com/thegalabs/go/errors/echo"
)

e := echo.New()
...
e.HTTPErrorHandler = errorsecho.DefaultHandler()
```
