package echo

import (
	"net/http"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"

	"gitlab.com/thegalabs/go/errors/mocks"
)

type Model struct {
	Email string `validate:"required,email"`
}

func TestValidatorHandler(t *testing.T) {
	model := &Model{"hello"}

	v := validator.New()
	err := v.Struct(model)

	context := new(mocks.Context)
	context.On("Response").Return(&echo.Response{Committed: false})
	context.On("JSON", http.StatusBadRequest, mock.AnythingOfType("Response")).Return(nil)

	DefaultHandler()(err, context)
}
