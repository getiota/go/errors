// Package echo provides a default translator for the echo server
package echo

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/thegalabs/go/errors"
)

// Translator allows translating echo errors
func Translator(err error) errors.HTTPError {
	if e, ok := err.(*echo.HTTPError); ok {
		return errors.NoResponse(e.Code)
	}

	return errors.DefaultTranslator(err)
}
