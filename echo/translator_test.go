package echo

import (
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/errors"
)

func TestTranslateEchoErro(t *testing.T) {
	err := Translator(echo.ErrBadRequest)

	noresp, ok := err.(errors.NoResponse)

	assert.True(t, ok)
	assert.Equal(t, echo.ErrBadRequest.Code, int(noresp))
}
