package errors

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetStatusCode(t *testing.T) {
	assert.Equal(t, http.StatusBadRequest, Response{}.GetStatusCode())
}

func TestJSONMarshal(t *testing.T) {
	response := Response{
		StatusCode: 403,
		Code:       "hello",
		Detail:     "bla",
	}

	bs, err := json.Marshal(response)
	assert.Nil(t, err)
	expected := `{"code":"hello","detail":"bla"}`
	assert.Equal(t, expected, string(bs))
}
