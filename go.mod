module gitlab.com/thegalabs/go/errors

go 1.16

require (
	github.com/go-playground/validator/v10 v10.11.0
	github.com/labstack/echo/v4 v4.7.2
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.1
)
